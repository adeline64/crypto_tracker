from crypt import methods
from flask import Flask, render_template, url_for, request

app = Flask(__name__)

# Config options - Make sure you created a 'config.py' file.
app.config.from_object('config')
# To get one variable, tape app.config['MY_VARIABLE']

@app.route('/', methods=['GET'])
@app.route('/accueil/', methods=['GET'])
def accueil():
    uid = request.args.get('id')
    nom_crypto = request.args.get('nom_crypto')
    quantite_actuel = request.args.get('quantite_actuel')
    gains = request.args.get('gains')
    img = 'http://graph.facebook.com/' + uid + '/picture?type=large'
    return render_template('accueil.html',
        nom_crypto=nom_crypto,
        quantite_actuel=quantite_actuel,
        gains=gains,
        img=img)

@app.route('/add/')
def add():
    uid = request.args.get('id')
    nom_crypto = 'http://graph.facebook.com/' + uid + '/picture?type=large'
    quantite_achete = request.args.get('quantite_achete')
    prix_achat = 'http://graph.facebook.com/' + uid + '/picture?type=large'
    return render_template('add.html',
        nom_crypto=nom_crypto,
        quantite_achete=quantite_achete,
        prix_achat=prix_achat)

@app.route('/prevewGraph/')
def prevewGraph():
    return render_template('prevewGraph.html')

@app.route('/remove/')
def remove():
    uid = request.args.get('id')
    nom_crypto = 'http://graph.facebook.com/' + uid + '/picture?type=large'
    quantite_actuel = request.args.get('quantite_actuel')
    return render_template('accueil.html',
        nom_crypto=nom_crypto,
        quantite_actuel=quantite_actuel)

if __name__ == "__main__":
    app.run()